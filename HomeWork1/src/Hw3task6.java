
import java.util.*;

    public class Hw3task6 {
        public static void main(String[] args) {
            int size = 7;
            int tmp;
            Random r = new Random();
            int[] arr = new int[size];
            for (int i = 0; i < size; i++) {
                arr[i] = r.nextInt();
                System.out.print(arr[i] + " ");
            }
            System.out.println(" ");
            for (int i = 0; i < size/2; i++) {
                tmp = arr[i];
                arr[i] = arr[size-1-i];
                arr[size-1-i] = tmp;
            }
            for (int i = 0; i < size; i++) {
                System.out.print(arr[i] + " ");
            }
        }
    }

