 class Calc {
    double calc(double secondValue,char operation,double firstValue) {
        switch (operation){
            case '*':return firstValue* secondValue;
            case '/':return firstValue/ secondValue;
            case '+':return firstValue+ secondValue;
            case '-':return firstValue - secondValue;
            case 's':return  Math.sqrt(firstValue * secondValue);
            case '%':return (firstValue/100) % secondValue ;
            case 'k':return  Math.sin(firstValue);
            case '&':return  Math.cos(firstValue);


            default:return 0;
        }
    }
}
