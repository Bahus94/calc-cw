import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


class Panel extends JPanel {

    private JTextField txt;
    private JButton bt1;
    private JButton bt2;
    private JButton bt3;
    private JButton bt4;
    private JButton bt5;
    private JButton bt6;
    private JButton bt7;
    private JButton bt8;
    private JButton bt9;
    private JButton bt0;
    private JButton bt_c;
    private JButton bt_del;
    private JButton bt_um;
    private JButton bt_plus;
    private JButton bt_minus;
    private JButton bt_res;
    private JButton bt_root;
    private JButton bt_percent;
    private JButton bt_cos;
    private  JButton bt_sin;
    private double firstValue = 0;
    private char operation = ' ';
     Panel (){
    setLayout(null);
    setBackground(Color.GREEN);
    init();
    add(txt);
    add(bt1);
    add(bt2);
    add(bt3);
    add(bt4);
    add(bt5);
    add(bt6);
    add(bt7);
    add(bt8);
    add(bt9);
    add(bt0);
    add(bt_res);
    add(bt_um);
    add(bt_c);
    add(bt_del);
    add(bt_minus);
    add(bt_plus);
    add(bt_root);
    add(bt_percent);
    add(bt_cos);
    add(bt_sin);
    Listener();
}
private void init(){
txt = new JTextField();
txt.setBounds(50,10,200,50);
txt.setFont(new Font("Courier New",Font.ITALIC,30));

bt1 = new JButton ("1");
bt1.setBounds(50,60,50,50);

bt2 = new JButton("2");
bt2.setBounds(100,60,50,50);

bt3 = new JButton("3");

bt3.setBounds(150,60,50,50);

 bt4 = new JButton("4");
 bt4.setBounds(200,60,50,50);

 bt5 = new JButton("5");
 bt5.setBounds(50,110,50,50);

 bt6 = new JButton("6");
 bt6.setBounds(100,110,50,50);

 bt7 = new JButton("7");
 bt7.setBounds(150,110,50,50);

 bt8 = new JButton("8");
 bt8.setBounds(200,110,50,50);

 bt9 = new JButton("9");
 bt9.setBounds(50,160,50,50);

 bt0 = new JButton("0");
 bt0.setBounds(100,160,50,50);

    bt_res = new JButton("res");
    bt_res.setBounds(150,160,50,50);

    bt_c = new JButton("C");
    bt_c.setBounds(200,160,50,50);

    bt_um = new JButton("*");
    bt_um.setBounds(250,10,50,50);

    bt_del = new JButton("/");
    bt_del.setBounds(250,60,50,50);

    bt_plus = new JButton("+");
    bt_plus.setBounds(250,110,50,50);

    bt_minus = new JButton("-");
    bt_minus.setBounds(250,160,50,50);

    bt_root = new JButton("s");
    bt_root.setBounds(300,110,50,50);

    bt_percent = new JButton("%");
    bt_percent.setBounds(300,60,50,50);

    bt_cos = new JButton("cos");
    bt_cos.setBounds(300,160,50,50);

    bt_sin = new JButton("sin");
    bt_sin.setBounds(300,10,50,50);
}
private void Listener() {

    bt1.addActionListener(e -> txt.setText(txt.getText() + 1));

    bt2.addActionListener(e -> txt.setText(txt.getText() + 2));

    bt3.addActionListener(e -> txt.setText(txt.getText() + 3));

    bt4.addActionListener(e -> txt.setText(txt.getText() + 4));

    bt5.addActionListener(e -> txt.setText(txt.getText() + 5));

    bt6.addActionListener(e -> txt.setText(txt.getText() + 6));

    bt7.addActionListener(e -> txt.setText(txt.getText() + 7));

    bt8.addActionListener(e -> txt.setText(txt.getText() + 8));

    bt9.addActionListener(e -> txt.setText(txt.getText() + 9));

    bt0.addActionListener(e -> txt.setText(txt.getText() + 0));

    bt_c.addActionListener(e -> {
        String temp = txt.getText();
        txt.setText(temp.substring(0, temp.length() - 1));
    });

    bt_plus.addActionListener(e -> {
        firstValue = Double.valueOf(txt.getText());
        txt.setText("");
        operation = '+';
    });
    bt_minus.addActionListener(e -> {
        firstValue = Double.valueOf(txt.getText());
        txt.setText("");
        operation = '-';
    });
    bt_del.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            firstValue = Double.valueOf(txt.getText());
            txt.setText("");
            operation = '/';
        }
    });
    bt_um.addActionListener(e -> {
        firstValue = Double.valueOf(txt.getText());
        txt.setText("");
        operation = '*';
    });

    txt.addActionListener(e -> {

    });
    bt_root.addActionListener(e -> {
        firstValue = Double.valueOf(txt.getText());
        txt.setText("");
        operation = 's';
    });
    bt_percent.addActionListener(e -> {
        firstValue = Double.valueOf(txt.getText());
        txt.setText("");
        operation = '%';
    });
    bt_cos.addActionListener(e -> {
        firstValue = Double.valueOf(txt.getText());
        txt.setText("");
        operation = 'k';
    });
    bt_sin.addActionListener(e -> {
        firstValue = Double.valueOf(txt.getText());
        txt.setText("");
        operation = '&';
    });
    bt_res.addActionListener(e -> {
        double secondValue = Integer.valueOf(txt.getText());
        if (operation == '/' && secondValue == 0) {
            JOptionPane.showMessageDialog(null, "делить на ноль нельзя");
        }
        txt.setText("" + new Calc().calc(secondValue, operation, firstValue));
    });

    }
}





