import java.io.*;

public class Main {
    public static void main(String[] args) {

        try {
            File file = new File("file.txt");
            InputStreamReader inputStreamReader = new InputStreamReader(System.in);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            FileWriter fileReader = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileReader);
            HW4 hw4 = new HW4();
            hw4.DayOfWeek(5);
            hw4.convertNumberToStr(8);
            hw4.convertStrToNumber("тридцать пять");
            hw4.twoDimensionalSpace(3,5,2,4);
            String line;
            while(!(line = bufferedReader.readLine()).equals("exit")) {
                bufferedWriter.write(line);
            }

            bufferedReader.close();
            bufferedWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }



    }
}